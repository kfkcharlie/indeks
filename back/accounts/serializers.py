from rest_framework import serializers

from .models import UserStudent, Setting
from polytechnic.serializers import FacultySerializer


class SettingSerializer(serializers.ModelSerializer):
    faculty = FacultySerializer()
    study_degree = serializers.CharField(source='get_study_degree_display')
    
    class Meta:
        model = Setting
        fields = ['pk', 'faculty', 'study_degree', 'is_active']


class UserStudentSerializer(serializers.ModelSerializer):
    faculty = FacultySerializer()
    study_degree = serializers.CharField(source='get_study_degree_display')
    setting = SettingSerializer()
    
    class Meta:
        model = UserStudent
        fields = ['pk', 'name', 'last_name', 'is_active', 'setting', 'date_created']