from django.contrib.auth.models import User
from django.db import models
from polytechnic import models as m_polytechnic     # Odwołanie do modelu w innej aplikacji
from apka_baza.consts import STUDY_DEGREES

# from django.utils import timezone

# Create your models here.
class UserStudent(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    # Ponoć Django sam tworzy samoinkrementujące się kolumny (pola) id
    account = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=50)
    login = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)  # tworzy się automatycznie przy utworzeniu
    # Na razie zakomentowane bo nie potrzebujemy tego do zdania a nie wiem jak to działa w DJango
    # W dodatku nie jestem pewien czy dobrze podepnę model pomiędzy aplikacjami
    # idRole = models.ForeignKey(max_length=50)

    # Metody / Funkcje klasy
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'

class Setting(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    user = models.OneToOneField(UserStudent, on_delete=models.CASCADE, primary_key=True)
    faculty = models.ForeignKey(m_polytechnic.Faculty, on_delete=models.CASCADE, help_text="Kierunek studiów")                 # Odwołanie do modelu w app polytechnic
    study_degree = models.IntegerField(choices=STUDY_DEGREES, help_text='Poziom studiów')
    is_active = models.BooleanField(default=True)
    # idRole = models.ForeignKey(max_length=50)  Dict_DegreeOfStudies

    # Metody / Funkcje klasy
