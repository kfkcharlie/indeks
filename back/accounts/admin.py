from django.contrib import admin
from .models import UserStudent, Setting

# Register your models here.
admin.site.register(UserStudent)
admin.site.register(Setting)
