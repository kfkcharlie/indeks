FROM python:3

ENV PYTHONUNBUFFERED 1

ENV DB_USER postgres
ENV DB_DB db_indeks
ENV DB_PASSWORD root
ENV DB_HOST apkabazadb

RUN mkdir /code
WORKDIR /code
COPY req.txt /code/
RUN pip install -r req.txt
COPY . /code/