# INDEKS
![Indeks Logo](IndeksLogo.PNG)
REST API - umożliwia komunikację z bazą danych za pomocą zapytań http z wykorzystaniem metod PUT GET i POST
Baza danych jest stworzona na podstawiebudowy struktury uczelni, przydzielona do wydziału AEI Politechniki Śląskiej. Zawiera pytania i odpowiedzi na zapisane kartkówki, kolokwia i egzaminy przypisane do konkretnych przedmiotów, konkretnych kierunków dla wybranych poziomów toku studiów.

## Instalacja
 W celu uruchomienia projektu należy się zaopatrzyć w poniższe moduły:
 PostgreSQL 13.2
 Python 3.9.1
 DJango 3.1.6
 psycopg2 2.8.6
 virtualenv
 virtualenvwrapper

 Dokładny proces instalacji powyższych komponentów znajduje się w zamieszczonych linkach poniżej:
 - [Python](https://www.journaldev.com/30076/install-python-windows-10)
 - [DJango-win10](https://docs.djangoproject.com/en/3.1/howto/windows/#install-python-windows)
 - [PostgreSQL dla DJango](https://medium.com/@9cv9official/creating-a-django-web-application-with-a-postgresql-database-on-windows-c1eea38fe294)
 - [DJango Tutorial](https://docs.djangoproject.com/pl/3.1/intro/tutorial02/)

 ## API
 Po zainstalowaniu wszystkich komponentów można przejść do korzystania z API.
 W celu uruchomienia serwera należy:
 1. Uruchomić konsolę w folderze "back"
 2. Wywołać komendę:
 ```bash
    py manage.py runserver
 ```
 Poniżej lista endpointów możliwych do wywołania wraz z opisem zawartości odpowiedzi zwrotnej:
 - lista testów  ['aei/fields/subjects/classes/test/'](http://localhost:8000/aei/fields/subjects/classes/test/)
 - lista zajęć ['aei/fields/subjects/classes/'](http://localhost:8000/aei/fields/subjects/classes/)
 - lista przedmiotów ['aei/fields/subjects/'](http://localhost:8000/aei/fields/subjects/)
 - lista kierunków ['aei/fields/'](http://localhost:8000/aei/fields/)
 - lista komentarzy do odpowiedzi [fields/subjects/test/question/answer/comment/](http://localhost:8000/aei/fields/subjects/test/question/answer/comment/)
 - lista odpowiedzi na pytania [fields/subjects/test/question/answer/](http://localhost:8000/aei/fields/subjects/test/question/answer/comment/)
 - lista pytań [fields/subjects/test/question/](http://localhost:8000/aei/fields/subjects/test/question/answer/comment/)
 Podając numer po ostatnim '/' jest on brany jako ID i wyświetla wybrany element bazy o podanym ID, przykłady:
 - kierunek o id 1 ['aei/fields/1'](http://localhost:8000/aei/fields/1)
 - test o id 1  ['aei/fields/subjects/classes/test/1'](http://localhost:8000/aei/fields/subjects/classes/test/1)

 Wszystkie odpowiedzi zostają przekazane w formacie JSON.
 
### Przykład użycia API
![Przykład użycia API](API.png)





