
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Question, Answer, Comment
from .serializers import QuestionSerializer, AnswerSerializer, CommentSerializer

# Create your views here.
class QuestionList(APIView):
    """
    List all active, or create new
    """    
    serializer = QuestionSerializer
    model = Question

    def get(self, request, format=None):
        objs = QuestionList.model.objects.filter(is_active=True)
        serializer = QuestionList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = QuestionList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class QuestionDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = QuestionSerializer
    model = Question

    def get_object(self, pk):
        model = QuestionDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = QuestionDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = QuestionDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AnswerList(APIView):
    """
    List all active, or create new
    """    
    serializer = AnswerSerializer
    model = Answer

    def get(self, request, format=None):
        objs = AnswerList.model.objects.filter(is_active=True)
        serializer = AnswerList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = AnswerList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = AnswerSerializer
    model = Answer

    def get_object(self, pk):
        model = AnswerDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = AnswerDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = AnswerDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CommentList(APIView):
    """
    List all active, or create new
    """    
    serializer = CommentSerializer
    model = Comment

    def get(self, request, format=None):
        objs = CommentList.model.objects.filter(is_active=True)
        serializer = CommentList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = CommentList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = CommentSerializer
    model = Comment

    def get_object(self, pk):
        model = CommentDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = CommentDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = CommentDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)