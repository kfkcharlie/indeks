from rest_framework import serializers

from .models import Question, Answer, Comment


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'pk', 'content', 'date_created', 'date_edited', 'edited', 'is_active'
        ]


class AnswerSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(source='comment_set', many=True, read_only=True)

    class Meta:
        model = Answer
        fields = [
            'pk', 'content', 'best_answer', 'comments', 'date_created', 'date_edited', 
            'edited', 'is_active'
        ]


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(source='answer_set', many=True, read_only=True)

    class Meta:
        model = Question
        fields = [
            'pk', 'test', 'content', 'answers', 'date_created', 'date_edited', 
            'edited', 'is_active'
        ]

