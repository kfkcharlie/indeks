from django.urls import path
from forum import views

app_name = 'forum'

urlpatterns = [
    path('fields/subjects/test/question/answer/comment/', views.CommentList.as_view()),
    path('fields/subjects/test/question/answer/comment/<int:pk>/', views.CommentDetail.as_view()),
    path('fields/subjects/test/question/answer/', views.AnswerList.as_view()),
    path('fields/subjects/test/question/answer/<int:pk>/', views.AnswerDetail.as_view()),
    path('fields/subjects/test/question/', views.QuestionList.as_view()),
    path('fields/subjects/test/question/<int:pk>/', views.QuestionDetail.as_view()),
]