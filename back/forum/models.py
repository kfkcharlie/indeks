from django.db import models
from polytechnic import models as m_polytechnic                                     # Odwołanie do modelu w innej aplikacji
from accounts import models as m_accounts                                           # Odwołanie do modelu w innej aplikacji

# Create your models here.
class Question(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    # Ponoć Django sam tworzy samoinkrementujące się kolumny (pola) id
    test = models.ForeignKey(m_polytechnic.Test, on_delete=models.CASCADE)
    content = models.CharField(max_length=1000)
    user = models.ForeignKey(m_accounts.UserStudent, on_delete=models.CASCADE, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)  # tworzy się automatycznie przy utworzeniu
    date_edited = models.DateTimeField(auto_now=True)  # aktualizuje się automatycznie przy zapisie
    edited = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    
    
class Answer(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    question = models.ForeignKey(Question, on_delete=models.CASCADE)             # Odwołanie do tabeli z aplikacji
    content = models.CharField(max_length=1000)
    user = models.ForeignKey(m_accounts.UserStudent, on_delete=models.CASCADE, blank=True, null=True)
    best_answer = models.BooleanField()
    date_created = models.DateTimeField(auto_now_add=True)  # tworzy się automatycznie przy utworzeniu
    date_edited = models.DateTimeField(auto_now=True)  # aktualizuje się automatycznie przy zapisie
    edited = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)


class Comment(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    # Ponoć Django sam tworzy samoinkrementujące się kolumny (pola) id
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)             # Odwołanie do tabeli z aplikacji
    content = models.CharField(max_length=1000)
    user = models.ForeignKey(m_accounts.UserStudent, on_delete=models.CASCADE, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)  # tworzy się automatycznie przy utworzeniu
    date_edited = models.DateTimeField(auto_now=True)  # aktualizuje się automatycznie przy zapisie
    edited = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
