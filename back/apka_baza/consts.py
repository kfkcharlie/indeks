DEGREE_ENGINEER = 0
DEGREE_MASTER = 1
DEGREE_DOCTOR = 2

STUDY_DEGREES = (
    (DEGREE_ENGINEER, "inż."),
    (DEGREE_MASTER, "mgr"),
    (DEGREE_DOCTOR, "dr"), 
)

CLASS_EX = 0
CLASS_LAB = 1
CLASS_LECTURE = 2

CLASSES = (
    (CLASS_EX, "ćwiczenia"),
    (CLASS_LAB, "laboratiorium"),
    (CLASS_LECTURE, "wykłady"),
)

TEST_SHORT = 0
TEST_MID = 1
TEST_END = 2

TESTS = (
    (TEST_SHORT, "kartkówka"),
    (TEST_MID, "kolokwium"),
    (TEST_END, "egzamin"),
)

ROLE_STUDENT = 0
ROLE_REVIEW = 1
ROLE_ADMIN = 2

ROLES = (
    (ROLE_STUDENT, "student"),
    (ROLE_REVIEW, "recenzent"),
    (ROLE_ADMIN, "admin"),
)