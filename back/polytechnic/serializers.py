from rest_framework import serializers

from polytechnic.models import Faculty, Subject, Class, Test
from forum.serializers import QuestionSerializer

class FacultySerializer(serializers.ModelSerializer):
    study_degree = serializers.CharField(source='get_study_degree_display')
    
    class Meta:
        model = Faculty
        fields = ['pk', 'name', 'study_degree', 'is_active']


class SubjectSerializer(serializers.ModelSerializer):
    faculty = FacultySerializer()
    
    class Meta:
        model = Subject
        fields = ['pk', 'name', 'faculty', 'description', 'is_active']


class ClassSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer()
    type = serializers.CharField(source='get_type_display')
    
    class Meta:
        model = Class
        fields = ['pk', 'subject', 'type', 'semester', 'description', 'is_active']


class TestSerializer(serializers.ModelSerializer):
    _class = ClassSerializer()
    type = serializers.CharField(source='get_type_display')
    questions = QuestionSerializer(source='question_set', many=True, read_only=True)
    
    class Meta:
        model = Test
        fields = ['pk', '_class', 'type', 'number', 'description', 'questions', 'is_active']
