from django.urls import path
from polytechnic import views

app_name = 'polytechnic'

urlpatterns = [
    path('fields/subjects/classes/<int:pk>/test/', views.ClassessTestList.as_view()),
    path('fields/subjects/classes/test/', views.TestList.as_view()),
    path('fields/subjects/classes/test/<int:pk>/', views.TestDetail.as_view()),
    path('fields/subjects/<int:pk>/classes/', views.SubjectsClassList.as_view()),
    path('fields/subjects/classes/', views.ClassList.as_view()),
    # path('fields/subjects/classes/<int:pk>/', views.ClassDetail.as_view()),

    ### zamiast tych dwóch można wrzucić to wyżej
    # path('fields/subjects/<int:pk>/test/', views.SubjectsTestList.as_view()),
    path('fields/subjects/test/', views.TestList.as_view()),
    path('fields/subjects/test/<int:pk>/', views.TestDetail.as_view()),
    ###
    path('fields/subjects/', views.SubjectList.as_view()),
    path('fields/subjects/<int:pk>/', views.SubjectDetail.as_view()),
    path('fields/', views.FacultyList.as_view()),
    path('fields/<int:pk>/', views.FacultyDetail.as_view()),
    path('fields/<int:pk>/subjects/', views.FacultySubjectList.as_view()), # New
] 