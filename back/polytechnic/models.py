from django.db import models
from apka_baza.consts import STUDY_DEGREES, CLASSES, TESTS

# Create your models here.
class Faculty(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    # Ponoć Django sam tworzy samoinkrementujące się kolumny (pola) id
    name = models.CharField(max_length=100)                                         # Jak zrobić walidację NIEakceptującą znaków specjalnych?
    study_degree = models.IntegerField(choices=STUDY_DEGREES, help_text='Poziom studiów')
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Faculties'


class Subject(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    name = models.CharField(max_length=100)                                         # Minimalna liczba znaków określona zostaje w FORMULARZu!
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)      # Odwołanie do tabeli z aplikacji
    description = models.CharField(max_length=1000)                                 # Testowo 1.000 znaków limitu
    is_active = models.BooleanField(default=True)

    # def add():
    #     #ciało funkji


class Class(models.Model): 
    # Pola modelu / kolumny w bazie / zmienne
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)               # Odwołanie do tabeli z aplikacji
    type = models.IntegerField(choices=CLASSES, help_text='Typ zajęć')
    semester = models.IntegerField()
    description = models.CharField(max_length=1000)                                 # Testowo 1.000 znaków limitu
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Classes'


class Test(models.Model):
    # Pola modelu / kolumny w bazie / zmienne
    _class = models.ForeignKey(Class, on_delete=models.CASCADE)         # Odwołanie do tabeli z aplikacji
    number = models.IntegerField()
    type = models.IntegerField(choices=TESTS, help_text='Typ testu')
    description = models.CharField(max_length=1000)                                 # Testowo 1.000 znaków limitu
    is_active = models.BooleanField(default=True)
