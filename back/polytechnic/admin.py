from django.contrib import admin
from .models import Faculty, Subject, Class, Test

# Register your models here.
admin.site.register(Faculty)
admin.site.register(Subject)
admin.site.register(Class)
admin.site.register(Test)
