from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Faculty, Subject, Class, Test
from .serializers import FacultySerializer, SubjectSerializer, ClassSerializer, TestSerializer


class FacultyList(APIView):
    """
    List all active, or create new
    """    
    serializer = FacultySerializer
    model = Faculty

    def get(self, request, format=None):
        objs = FacultyList.model.objects.filter(is_active=True)
        serializer = FacultyList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = FacultyList.serializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FacultyDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = FacultySerializer
    model = Faculty

    def get_object(self, pk):
        model = FacultyDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = FacultyDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = FacultyDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class SubjectList(APIView):
    """
    List all active, or create new
    """    
    serializer = SubjectSerializer
    model = Subject

    def get(self, request, format=None):
        objs = SubjectList.model.objects.filter(is_active=True)
        serializer = SubjectList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = SubjectList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FacultySubjectList(APIView):
    """
    List all active, or create new
    """    
    serializer = SubjectSerializer
    model = Subject

    def get(self, request, pk, format=None):
        objs = SubjectList.model.objects.filter(faculty__pk=pk,is_active=True)
        serializer = SubjectList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = SubjectList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SubjectDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = SubjectSerializer
    model = Subject

    def get_object(self, pk):
        model = SubjectDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = SubjectDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = SubjectDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ClassList(APIView):
    """
    List all active, or create new
    """    
    serializer = ClassSerializer
    model = Class

    def get(self, request, format=None):
        objs = ClassList.model.objects.filter(is_active=True)
        serializer = ClassList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = ClassList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SubjectsClassList(APIView):
    """
    List all active, or create new
    """    
    serializer = ClassSerializer
    model = Class

    def get(self, request, pk, format=None):
        objs = ClassList.model.objects.filter(subject__pk=pk,is_active=True)
        serializer = ClassList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = ClassList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClassDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = ClassSerializer
    model = Class

    def get_object(self, pk):
        model = ClassDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = ClassDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = ClassDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TestList(APIView):
    """
    List all active, or create new
    """    
    serializer = TestSerializer
    model = Test

    def get(self, request, format=None):
        objs = TestList.model.objects.filter(is_active=True)
        serializer = TestList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = TestList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClassessTestList(APIView):
    """
    List all active, or create new
    """    
    serializer = TestSerializer
    model = Test

    def get(self, request, pk, format=None):
        objs = TestList.model.objects.filter(_class__pk=pk, is_active=True)
        serializer = TestList.serializer(objs, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        serializer = TestList.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TestDetail(APIView):
    """
    Retrieve, update or delete a instance.
    """
    serializer = TestSerializer
    model = Test

    def get_object(self, pk):
        model = TestDetail.model
        try:
            return model.objects.get(pk=pk, is_active=True)
        except model.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        obj = self.get_object(pk)
        serializer = TestDetail.serializer(obj)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                "You are not allowed to perform this action", 
                status=status.HTTP_403_FORBIDDEN
            )

        serializer = TestDetail.serializer(obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"error": "Log in"}, status=status.HTTP_403_FORBIDDEN)

        obj = self.get_object(pk)
        if obj.user.account != request.user:
            return Response(
                {"error": "You are not allowed to perform this action"}, 
                status=status.HTTP_403_FORBIDDEN
            )

        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)