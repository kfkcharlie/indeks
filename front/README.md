# Frontend

## Spis Treści
* Opis ogólny działania
  * Wymagane oprogramowanie i setup środowiska
  * Uruchamienie aplikacji
  * Pomoc

* Struktura programu
  * Wygląd i nawigacja
  * Strony
    * Strona główna
    * Edycja Notatek
    * Wyszukiwanie

* Podział plików
  * Odczyt .json otrzymywanego z serwera Django
  * Ustawienie serwera proxy
  * Budowa wzorcowego okienka, w którym wyświetlane są elementy
  * Boczny pasek nawigacji oraz górny pasek z nazwą strony
    * Main-Layout.html





## Opis ogólny działania

Aplikacja od strony Frontend składa się z widoku strony głównej oraz podstron o podobnej strukturze, dzięki którym w łatwy sposób można odnajdywać szukane materiały z bazy danych. Z widoku wysyłane są zapytania do serwera Django (przez serwer proxy), a z powrotem otrzymywane są serializowane dane w formacie .json, na podstawie których wypełniane są podstrony.

Aplikacja dodatkowo umożliwa korzystanie z OpenMap. Na stronie głównej zaznaczony jest adres wydziału AEI.

### Wymagane oprogramowanie i setup środowiska
Projekt został stworzony z wykorzystaniem angulara 11

W pierwszej kolejności należy zainstalować node js i angulara:
- [node js](https://nodejs.org/en/)
- [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

Następnie w folderze docelowym w bashu wpisać komendę:
`ng new indeks --style=scss` 

Następnie w terminalu do lokalizacji projektu doinstalować komendami:
- [bulma](https://bulma.io/) 
w cmd w lokalizacji projektu: `npm install bulma --save`

- moduł bootstrap `npm install @angular/Ngb --save`

### Uruchamienie aplikacji
Dla celów łączenia ze stroną backendową należy skorzystać z wesji build uruchamianej komendą `npm start`
(w celu stworzenia serwera proxy, w innym wypadku dostęp nie zostanie przyznany)

Dla podglądu widoków można użyć komendy `ng serve`, która tworzy serwer podd adresem `http://localhost:4200/`
// jednak dostęp do osobnej domeny django

### Pomoc
wpisać komendę `ng help`
lub skorzystać z linka [Angular CLI Overview and Command Reference](https://angular.io/cli)

## Struktura programu

### Wygląd i nawigacja

Strony i ich ścieżki (w domenie `http://localhost:4200/`):
* Główna local:  `''`

* Notatki                `'topiclist'`  - /pages/komponent topic-list
* EdycjaNotatek         `'topiclist/:id'` - /pages/omponent topic-details
* Nowa Notatka           `'topiclist/new'` - /pages/komponent topic-details


* Kierunki               `'fieldsofstudy'` - komponent /pages/fields-of-study.component
* Przedmioty             `'fieldsofstudy/:id/subjects'` - komponent /paths/subcjet-list.component
* Rodzaj zajęć           `'fieldsofstudy/:Studyid/subjects/:id/classes'` - komponent /pages/classes-list-component
* Lista testów           `'fieldsofstudy/:Studyid/subjects/:Classid/classes/:id/tests'` - komponent /pages/tests-list.component
* Lista pytań do testu   `'fieldsofstudy/:Studyid/subjects/:Classid/classes/:Testid/tests/:id/:type'` - komponent /pages/test-details.component

### Strony
#### Strona główna
Na głównej stronie znajduje się okienko z Open Street Map
Widać też pasek nawigacji po lewej stronie - główna funkcjonalność kryje się pod przyciskiem
kierunki, gdzie będziemy przemieszczać się po podstronach w celu znalezienia materiałów

![Main page](src/assets/md_imgs/01_mainp.png)

#### Edycja Notatek
Podstrona, na której użytkownik może edytować swoje notatki

![Edit note](src/assets/md_imgs/02_noteEdit.png)

#### Wyszukiwanie
##### Po kierunkach -> przedmiotach -> zajęciach -> testach -> PYTANIA + ODPOWIEDZI

![Wyszukane testy](src/assets/md_imgs/03_testy.png)


## Podział plików
Projekt składa się z komponentów, na które składa się zestaw: 
template.html   - templatka strony w html 
 style.scss     - styl w simple css
  script.ts     - typescript


### Odczyt .json otrzymywanego z serwera Django

W plikach `.ts` podstron odczyt dokonywany jest w czterech głównych krokach:
1. Wyznaczenie ID obiektu ze strony nadrzędnej
```
getParentID(){
    this.route.params.subscribe((params: Params) => {
      this.parentFieldID = params.id;
    });
    return this.parentFieldID;
  }
```

2. Odczytanie obiektu parent
```
getParentRow(id:number){
    id = id - 1;
    this.httpClient.get<any>('/aei/fields/subjects/?format=json').subscribe(
  response => {
    this.parentField = response[id];
  });    
}
```


3. Odczytanie obiektu child
```
getAllFields(parentID:number){
  let url: string = '/aei/fields/subjects/' + parentID + '/classes/?format=json';
  this.httpClient.get<any>(url).subscribe(
    response => {
      this.childrenFields = response;
      console.log(this.childrenFields);
    }); 
  
}
```

### Ustawienie serwera proxy
W celu odpytywania serwera Django

##### Nowy plik proxy.conf.json z targetem na Django
```
{
    "/aei": {
      "target": "http://localhost:8000",
      "secure": false,
      "changeOrigin": true
    }
  }
```

##### w pliku package.json zmiana opcji start
`"start": "ng serve --proxy-config proxy.conf.json"`


### Budowa wzorcowego okienka, w którym wyświetlane są elementy

```
<div #container class="topic-box-container">
<a [routerLink]="link_box">
    <div class="topic-box-content">
        <h1 class="topic-box-title">{{title}}</h1>

        <div #bodyText class="topic-box-body">
            <p>{{ body }}</p>
            <div #fadeout class="fade-out-overflow"></div>
        </div>        
    </div>
</a>
    

    <div class="x-button" (click)="DeleteTopicClick()"></div>

</div>
```
![Okienko](src/assets/md_imgs/04_okienko.PNG)

### Boczny pasek nawigacji oraz górny pasek z nazwą strony
#### Main-Layout.html
```
<div class="layout-box">
    
    <div class="top-bar">
        <h1 (click)="toMain()">Indeks.pl</h1>
        <div class="login-button" type="button" (click)="loginRedirect()"><h2>Logowanie</h2></div>
    </div>

    <div class="nav-content-container">

        <div class="nav-bar" *ngIf="isToggled">
            <div class="nav-title-bar">{{Department}}</div>
            <div class="nav-button" type="button" (click)="redirect('')"><h2>Strona główna</h2></div>
            <div class="nav-button" type="button" (click)="redirect('fieldsofstudy')"><h2>Kierunki</h2></div>
            <div class="nav-button" type="button" (click)="redirect('topiclist')"><h2>Notatki</h2></div>

        </div>
    
        <div class="page-content">
            <router-outlet></router-outlet>
        </div>      
        
    </div> 

</div>
```


