import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopicListComponent } from './pages/topic-list/topic-list.component';
import { MainLayoutComponent } from './pages/main-layout/main-layout.component';
import { TopicBoxComponent } from './topic-box/topic-box.component';
import { TopicDetailsComponent } from './pages/topic-details/topic-details.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegisterNewUserComponent } from './pages/register-new-user/register-new-user.component';
import { LoginUserComponent } from './pages/login-user/login-user.component';
import { UserPanelComponent } from './pages/user-panel/user-panel.component';
import { UserSettingsComponent } from './pages/user-settings/user-settings.component';
import { FieldsOfStudyComponent } from './pages/fields-of-study/fields-of-study.component';
import { FieldBoxComponent } from './pages/fields-of-study/field-box/field-box.component';
import { SubcjetListComponent } from './paths/subcjet-list/subcjet-list.component';
import { SubjectBoxComponent } from './paths/subject-list/subject-box/subject-box.component';
import { ClassesListComponent } from './pages/classes-list/classes-list.component';
import { ClassBoxComponent } from './pages/classes-list/class-box/class-box.component';
import { TestsListComponent } from './pages/tests-list/tests-list.component';
import { TestDetailsComponent } from './pages/test-details/test-details.component';

@NgModule({
  declarations: [
    AppComponent,
    TopicListComponent,
    MainLayoutComponent,
    TopicBoxComponent,
    TopicDetailsComponent,
    MainPageComponent,
    PageNotFoundComponent,
    RegisterNewUserComponent,
    LoginUserComponent,
    UserPanelComponent,
    UserSettingsComponent,
    FieldsOfStudyComponent,
    FieldBoxComponent,
    SubcjetListComponent,
    SubjectBoxComponent,
    ClassesListComponent,
    ClassBoxComponent,
    TestsListComponent,
    TestDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
