import { Component, OnInit } from '@angular/core';
import { FieldOfStudy } from '../../pages/fields-of-study/fields-of-study.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

export class Subject {
  constructor(
    
  ){}
  public pk: number;
  public name: string;
  public faculty: FieldOfStudy;
  public description: string;
  public is_active: boolean;
  
}

// {
//   "pk": 1,
//   "name": "Modelowanie Cyfrowe",
//   "faculty": {
//       "pk": 1,
//       "name": "Informatyka",
//       "study_degree": "mgr",
//       "is_active": true
//   },
//   "description": "Opis",
//   "is_active": true
// }

@Component({
  selector: 'app-subcjet-list',
  templateUrl: './subcjet-list.component.html',
  styleUrls: ['./subcjet-list.component.scss']
})
export class SubcjetListComponent implements OnInit {

  parentField: FieldOfStudy;
  parentFieldID: number;
  childrenFields: Subject[];

  constructor(private router: Router, private route: ActivatedRoute,private httpClient: HttpClient) { }
  

  ngOnInit(): void {

    this.getParentID();
    this.getParentRow(this.parentFieldID);

    this.getAllFields(this.parentFieldID);


    

    
  }

  getParentID(){
    this.route.params.subscribe((params: Params) => {
      this.parentFieldID = params.id;
    });
    return this.parentFieldID;
  }

  getParentRow(id:number){
        id = id - 1;

        this.httpClient.get<any>('/aei/fields/?format=json').subscribe(
      response => {
        this.parentField = response[id];
        console.log(this.parentField);
      });    
  }

  getAllFields(parentID:number){

    let url: string = '/aei/fields/' + parentID + '/subjects/?format=json';
    console.log(url);

    this.httpClient.get<any>(url).subscribe(
      response => {
        this.childrenFields = response;
        console.log(this.childrenFields);
      }); 
    
  }

  redirect(url:string){
    this.router.navigateByUrl('/topiclist')
  }



  

}
