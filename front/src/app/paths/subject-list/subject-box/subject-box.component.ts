import { Component, ElementRef, Input, AfterViewInit, Renderer2, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-subject-box',
  templateUrl: './subject-box.component.html',
  styleUrls: ['./subject-box.component.scss']
})

export class SubjectBoxComponent implements AfterViewInit {

  @Input() title: string;
  @Input() body: string;
  @Input() link_box: string;

  @Output() deleteTopic = new EventEmitter<void>();
  
  @ViewChild('fadeout', {static: true}) fadeout: ElementRef<HTMLElement>;
  @ViewChild('bodyText', {static: true}) bodyText: ElementRef<HTMLElement>;
  constructor(private renderer: Renderer2) { }

  ngAfterViewInit(): void {
    // work out if there is actual overflow and only then show fadeout
    let style = window.getComputedStyle(this.bodyText.nativeElement, null);
    let viewableHeight = parseInt(style.getPropertyValue("height"),10);
    if (this.bodyText.nativeElement.scrollHeight > viewableHeight){
      this.renderer.setStyle(this.fadeout.nativeElement, 'display', 'block');
    }
    else{
      this.renderer.setStyle(this.fadeout.nativeElement, 'display', 'none');
    }
  }

  DeleteTopicClick() {
    this.deleteTopic.emit();
  }

}
