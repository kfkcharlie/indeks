import { Injectable } from '@angular/core';
import { Topic } from './topic.model';

@Injectable({
  providedIn: 'root'
})
export class TopicsService {

  topics: Topic[] = new Array <Topic>();

  constructor() { }

  getAll(){
    return this.topics;
  }

  get (id: number){
    return this.topics[id];
  }

  getId(topic: Topic){
    return this.topics.indexOf(topic);
  }

  add(topic: Topic){
    //add a topic and return id of the topic where id=index
    let newLength = this.topics.push(topic);
    let index = newLength - 1;
    return index;
  }

  update(id: number, title: string, body: string){
    let topic = this.topics[id];
    topic.title = title;
    topic.body = body;
  }

  delete(id: number){
    this.topics.splice(id, 1);
  }
}
