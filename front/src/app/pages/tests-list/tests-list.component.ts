import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Class } from '../../pages/classes-list/classes-list.component';


export class Test {
  constructor(
    
  ){}
  public pk: number;
  public _class: Class;
  public type: string;
  public number: number;
  public description: string;
  public questions: string[];
  public is_active: boolean;
  
}

// {
//   "pk": 1,
//   "_class": {
//       "pk": 1,
//       "subject": {
//           "pk": 1,
//           "name": "Modelowanie Cyfrowe",
//           "faculty": {
//               "pk": 1,
//               "name": "Informatyka",
//               "study_degree": "mgr",
//               "is_active": true
//           },
//           "description": "Opis",
//           "is_active": true
//       },
//       "type": "ćwiczenia",
//       "semester": 1,
//       "description": "Opis",
//       "is_active": true
//   },
//   "type": "kartkówka",
//   "number": 1,
//   "description": "Opis",
//   "questions": [],
//   "is_active": true
// }

@Component({
  selector: 'app-tests-list',
  templateUrl: './tests-list.component.html',
  styleUrls: ['./tests-list.component.scss']
})
export class TestsListComponent implements OnInit {

  parentField: Class;
  parentFieldID: number;
  childrenFields: Test[];

  constructor(private router: Router, private route: ActivatedRoute,private httpClient: HttpClient) { }

  ngOnInit(): void {

    this.getParentID();
    this.getParentRow(this.parentFieldID);
    this.getAllFields(this.parentFieldID);


  }

  getParentID(){
    this.route.params.subscribe((params: Params) => {
      this.parentFieldID = params.id;
      console.log(this.parentFieldID);
    });
    return this.parentFieldID;
  }

  getParentRow(id:number){
    id = id - 1;

    this.httpClient.get<any>('/aei/fields/subjects/classes/?format=json').subscribe(
  response => {
    this.parentField = response[id];
    console.log(this.parentField);
  });    
}

getAllFields(parentID:number){

  let url: string = '/aei/fields/subjects/classes/' + parentID + '/test/?format=json';
  console.log(url);

  this.httpClient.get<any>(url).subscribe(
    response => {
      this.childrenFields = response;
      console.log(this.childrenFields);
    }); 
  
}

}
