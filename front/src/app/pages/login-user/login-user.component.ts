import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.scss']
})
export class LoginUserComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectRegister(): void {
    this.router.navigateByUrl('/register')
  }



}
