import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

export class FieldOfStudy {
  constructor(
    
  ){}
  public pk: number;
  public name: string;
  public study_degree: string;
  public is_active: boolean;
}

@Component({
  selector: 'app-fields-of-study',
  templateUrl: './fields-of-study.component.html',
  styleUrls: ['./fields-of-study.component.scss']
})
export class FieldsOfStudyComponent implements OnInit {

  fields: FieldOfStudy[];
  // testowa:string = '12';
  // index: string = this.testowa + '/subjects';
  constructor(
    private httpClient: HttpClient
  ) { }

 

  ngOnInit(): void {
    this.getAllFields();
  }



  getAllFields(){
    // return this.fields;
    this.httpClient.get<any>('/aei/fields/?format=json').subscribe(
      response => {
        console.log(response);
        this.fields = response;
      }
    );
  }

  getField(id:number){
    return this.fields[id];
  }

  getName(id:number){
    return this.fields[id].name;
  }

}
