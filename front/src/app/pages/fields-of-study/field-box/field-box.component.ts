import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-field-box',
  templateUrl: './field-box.component.html',
  styleUrls: ['./field-box.component.scss']
})
export class FieldBoxComponent implements OnInit {

  @Input() pk: number;
  @Input() name: string;
  @Input() study_degree: string;
  @Input() is_active: boolean;
  @Input() link_box: string;
  
  

  constructor() { }

  ngOnInit(): void {
  }

}
