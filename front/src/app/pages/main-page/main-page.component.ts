import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare const L:any;

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {

    var mapLangLat = [50.288661289194756, 18.677412856084292]
    var mymap = L.map('mapid').setView(mapLangLat, 15);
    var marker = L.marker(mapLangLat).addTo(mymap);
    marker.bindPopup("<b>Wydział AEI</b><br>").openPopup();


    //50.288661289194756, 18.677412856084292

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoienllbG9uaSIsImEiOiJja2xsZDd0Y2Ewa3V0MnVtczRma3d0eGU3In0.HhLA8hi8LqaOd5DUKr9gfA'
}).addTo(mymap);



    if (!navigator.geolocation){
      console.log('location is not supported');
    }

    navigator.geolocation.getCurrentPosition((position) =>
    console.log(
      `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
    ))
    // this.watchPosition();

  }

  watchPosition(){
    navigator.geolocation.watchPosition((position) =>{
    console.log(
      `lat: ${position.coords.latitude}, lon: ${position.coords.longitude}`
    );}, (err)=>{
      console.log(err);
    },{
      enableHighAccuracy: false,
      timeout: 5000,
      maximumAge: 0
    }
    )
  }
}
