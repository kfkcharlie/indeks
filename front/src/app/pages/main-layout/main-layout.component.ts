import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private router: Router) { }
  isToggled: boolean = true;

  ngOnInit(): void {
  }

  toMain(){
    this.router.navigateByUrl('/')
  }

  loginRedirect() {
    this.router.navigateByUrl('/login')
  }

  redirectTopics(){
    this.router.navigateByUrl('/topiclist')
    this.toggleNav();
  }

  redirect(url: string){
    this.router.navigateByUrl(url);
  }

  toggleNav() {
    this.isToggled = !this.isToggled;
  }

}
