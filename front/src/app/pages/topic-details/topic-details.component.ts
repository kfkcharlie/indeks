import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Topic } from 'src/app/sharedTopic/topic.model';
import { TopicsService } from 'src/app/sharedTopic/topics.service'

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.scss']
})
export class TopicDetailsComponent implements OnInit {

  topic: Topic;
  topicId: number;
  new: boolean;

  constructor(private topicsService: TopicsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    // find out if creating or editing existing one
    this.route.params.subscribe((params: Params) => {
      this.topic = new Topic();
      if (params.id){
        this.topic = this.topicsService.get(params.id);
        this.topicId = params.id;
        this.new = false;
      }
      else{
        this.new = true;
      }
    })

    
  }

  onSubmit(form: NgForm) {
    // save the topic
    if(this.new){
      this.topicsService.add(form.value);
    }
    else{
      this.topicsService.update(this.topicId, form.value.title, form.value.body);
    }
    this.router.navigateByUrl('/topiclist');
  }

  cancel(){
    this.router.navigateByUrl('/topiclist')
  }

}