import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Test } from '../../pages/tests-list/tests-list.component';
import { Class } from '../classes-list/classes-list.component';



export class Quiz {
  constructor(){}

  public pk: number;
  public _class: Class;
  public type: string;
  public number: number;
  public description: string;
  
  public questions: Quests[];

  public is_active: boolean;
  
}

export class Quests {
  constructor(){}

  public pk: number;
  public test: number;
  public content: string;

  public ans: Ans[];

  public date_created: Date;
  public date_edited: Date;
  public edited: boolean;
  public is_active: boolean;

}

export class Ans {
  constructor(){}

  public pk: number;
  public content: string;
  public best_answer: boolean;
  
  public comments: Comments[];

  public date_created: Date;
  public date_edited: Date;
  public edited: boolean;
  public is_active: boolean;

  
}

export class Comments{
  constructor(){}

  public pk: number;
  public content: string;

  public date_created: Date;
  public date_edited: Date;
  public edited: boolean;
  public is_active: boolean;

}



@Component({
  selector: 'app-test-details',
  templateUrl: './test-details.component.html',
  styleUrls: ['./test-details.component.scss']
})
export class TestDetailsComponent implements OnInit {

  parentField: Test;
  parentFieldID: number;
  childrenFields: Quiz;
  // childrenFields: any[];

  constructor(private router: Router, private route: ActivatedRoute,private httpClient: HttpClient) { }

  ngOnInit(): void {

    this.getParentID();
    // this.getParentRow(this.parentFieldID);
    this.getAllFields(this.parentFieldID);

  }

  getParentID(){
    this.route.params.subscribe((params: Params) => {
      this.parentFieldID = params.id;
      console.log(this.parentFieldID);
    });
    return this.parentFieldID;
  }

//   getParentRow(id:number){
//     id = id - 1;

//     this.httpClient.get<any>('/aei/fields/subjects/classes/test/'+id+'/?format=json').subscribe(
//   response => {
//     this.parentField = response[id];
//     // console.log(this.parentField);
//   });    
// }

getAllFields(parentID:number){

  let url: string = '/aei/fields/subjects/classes/test/' + parentID + '/?format=json';
  console.log(url);

  this.httpClient.get<any>(url).subscribe(
    response => {
      this.childrenFields = response;
      console.log(this.childrenFields);
    }); 

}
}
