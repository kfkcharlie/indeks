import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subject } from '../../paths/subcjet-list/subcjet-list.component';


export class Class {
  constructor(
    
  ){}
  public pk: number;
  public subject: Subject;
  public type: string;
  public semester: number;
  public description: string;
  public is_active: boolean;
  
}

// {
//   "pk": 1,
//   "subject": {
//       "pk": 1,
//       "name": "Modelowanie Cyfrowe",
//       "faculty": {
//           "pk": 1,
//           "name": "Informatyka",
//           "study_degree": "mgr",
//           "is_active": true
//       },
//       "description": "Opis",
//       "is_active": true
//   },
//   "type": "ćwiczenia",
//   "semester": 1,
//   "description": "Opis",
//   "is_active": true
// }

@Component({
  selector: 'app-classes-list',
  templateUrl: './classes-list.component.html',
  styleUrls: ['./classes-list.component.scss']
})
export class ClassesListComponent implements OnInit {

  parentField: Subject;
  parentFieldID: number;
  childrenFields: Class[];

  constructor(private router: Router, private route: ActivatedRoute,private httpClient: HttpClient) { }

  ngOnInit(): void {
    
    this.getParentID();
    this.getParentRow(this.parentFieldID);
    this.getAllFields(this.parentFieldID);

  }

  getParentID(){
    this.route.params.subscribe((params: Params) => {
      this.parentFieldID = params.id;
      console.log(this.parentFieldID);
    });
    return this.parentFieldID;
  }

  getParentRow(id:number){
    id = id - 1;

    this.httpClient.get<any>('/aei/fields/subjects/?format=json').subscribe(
  response => {
    this.parentField = response[id];
    console.log(this.parentField);
  });    
}

getAllFields(parentID:number){

  let url: string = '/aei/fields/subjects/' + parentID + '/classes/?format=json';
  console.log(url);

  this.httpClient.get<any>(url).subscribe(
    response => {
      this.childrenFields = response;
      console.log(this.childrenFields);
    }); 
  
}

}
