import { style, transition, trigger, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Topic } from 'src/app/sharedTopic/topic.model';
import { TopicsService } from 'src/app/sharedTopic/topics.service';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss'],
  animations: [
    trigger('itemAnim',[
      // entry animation
      transition('void => *', [
        style({
          height: 0,
          opacity: 0,
          transform: 'scale(0.85)',
          'margin-bottom': 0,

          // paddingTop: 0,
          // paddingBotom: 0,
          // paddingRight: 0,
          // paddingLeft: 0,
          padding: 0
        }),
        //firstly animation of spacings
        animate('50ms', style({
          height: '*',
          'margin-bottom': '*',

          // paddingTop: '*',
          // paddingBotom: '*',
          // paddingRight: '*',
          // paddingLeft: '*'
          padding: '*'
        })),
        animate(200)
      ])
    ])
  ]
})
export class TopicListComponent implements OnInit {

  topics: Topic[] = new Array<Topic>();
  filteredTopics: Topic[] = new Array<Topic>();

  constructor(private topicsService: TopicsService) { }

  ngOnInit(): void {
    this.topics = this.topicsService.getAll();
    this.filteredTopics = this.topics;
  }

  deleteTopic(id: number){
    this.topicsService.delete(id);
  }

  filter(query: string) {
    query = query.toLowerCase().trim();

    let allResults: Topic[] = new Array<Topic>();

    //split query into single words with spaces
    let terms: string[] = query.split(' ');
    // remove duplicates
    terms = this.removeDuplicates(terms);
    // compile all relevant results into output array
    terms.forEach(term => {
      let results = this.relevantTopics(term);
      //append results to allResults
      allResults = [...allResults, ...results];
    });

    this.filteredTopics = this.removeDuplicates(allResults);

  }

  removeDuplicates(arr: Array<any>): Array<any> {
    let uniqueResults: Set<any> = new Set<any>();
    // loop through data array
    arr.forEach(e => uniqueResults.add(e));

    return Array.from(uniqueResults);
  }

  relevantTopics(query:string){
    query = query.toLowerCase().trim();
    let relevantTopics = this.topics.filter(el => {
      if (el.title.toLocaleLowerCase().includes(query)){
        return true;
      }
      if (el.body && el.body.toLowerCase().includes(query)){
        return true;
      }
      return false;
    })

    return relevantTopics;
  }

  

}
