import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassesListComponent } from './pages/classes-list/classes-list.component';
import { FieldsOfStudyComponent } from './pages/fields-of-study/fields-of-study.component';
import { LoginUserComponent } from './pages/login-user/login-user.component';
import { MainLayoutComponent } from './pages/main-layout/main-layout.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { RegisterNewUserComponent } from './pages/register-new-user/register-new-user.component';
import { TestDetailsComponent } from './pages/test-details/test-details.component';
import { TestsListComponent } from './pages/tests-list/tests-list.component';
import { TopicDetailsComponent } from './pages/topic-details/topic-details.component';
import { TopicListComponent } from './pages/topic-list/topic-list.component';
import { UserPanelComponent } from './pages/user-panel/user-panel.component';
import { UserSettingsComponent } from './pages/user-settings/user-settings.component';
import { SubcjetListComponent } from './paths/subcjet-list/subcjet-list.component';

const routes: Routes = [
  {path: '', component: MainLayoutComponent, children:[
    {path: '', component: MainPageComponent},
    {path: 'register', component: RegisterNewUserComponent},
    {path: 'login', component: LoginUserComponent},
    {path: 'userpanel', component: UserPanelComponent},
    {path: 'usersettings', component: UserSettingsComponent},
    {path: 'fieldsofstudy', component: FieldsOfStudyComponent},
    {path: 'fieldsofstudy/:id/subjects', component: SubcjetListComponent},
    {path: 'fieldsofstudy/:Studyid/subjects/:id/classes', component: ClassesListComponent},
    {path: 'fieldsofstudy/:Studyid/subjects/:Classid/classes/:id/tests', component: TestsListComponent},
    {path: 'fieldsofstudy/:Studyid/subjects/:Classid/classes/:Testid/tests/:id/:type', component: TestDetailsComponent},
    {path: 'topiclist', component: TopicListComponent},
    {path: 'topiclist/new', component: TopicDetailsComponent},
    {path: 'topiclist/:id', component: TopicDetailsComponent},
    {path: '**', component: PageNotFoundComponent}
  ]}
];

//      /fieldsofstudy/1/subjects/1/classes/1/tests/1/kartkówka

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
